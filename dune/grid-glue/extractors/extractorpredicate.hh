// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
/*
 *  Filename:    extractorpredicate.hh
 *  Version:     1.0
 *  Created on:  Mar 10, 2009
 *  Author:      Gerrit Buse
 *  ---------------------------------
 *  Project:     dune-grid-glue
 *  Description: simple uniform descriptor for surface or mesh parts
 *
 */
/**
 * @file
 * @brief Base class for predicates selecting the part of a grid to be extracted
 */

#ifndef DUNE_GRIDGLUE_EXTRACTORS_EXTRACTORPREDICATES_HH
#define DUNE_GRIDGLUE_EXTRACTORS_EXTRACTORPREDICATES_HH

namespace Dune {

  namespace GridGlue {


/** \brief Base class for subentity-selecting predicates
    \tparam GV GridView that the subentities are extracted from
 */
template<typename GV, int codim>
class ExtractorPredicate
{
public:
  typedef typename GV::Traits::template Codim<0>::Entity Element;

  /** \brief Return true if a subentity should be extracted.
      \param element An element
      \param subentity Subentity number
   */
  virtual bool contains(const Element& element, unsigned int subentity) const = 0;

  /** \brief Dummy virtual destructor */
  virtual ~ExtractorPredicate() {}
};

}  // namespace GridGlue

}  // namespace Dune

#endif // DUNE_GRIDGLUE_EXTRACTORS_EXTRACTORPREDICATES_HH
